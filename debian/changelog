vtprint (2.0.2-15) unstable; urgency=medium

  * [657d291] d/control: fix VCS-Git
  * [a8b1900] use DEP-14 branch names
  * [0f0580d] bump std-ver to 4.6.1; no changes
  * [a65e217] use secure URL for homepage
  * [a70b3a7] d/copyright: convert to machine-readable format
  * [8cf9532] fix capitalization of Vcs header names

 -- Joseph Nahmias <jello@debian.org>  Wed, 26 Oct 2022 23:49:30 -0400

vtprint (2.0.2-14) unstable; urgency=medium

  * [79164a1] Resume maintenance, tweak maintainer name (Closes: 869295)
  * [8217993] d/control: update VCS-* to salsa URLs
  * [eabbcfd] Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: 939613)
  * [e708c3a] switch to debhelper compat 13
  * [d600a51] enable all hardening options
  * [7177cd0] set Rules-Requires-Root: no
  * [6062747] bump std-ver to 4.5.0, no changes
  * [f3fdcbb] add missing #include's
  * [1200f0a] use salsa-ci pipeline

 -- Joseph Nahmias <jello@debian.org>  Sun, 18 Oct 2020 01:43:15 -0400

vtprint (2.0.2-13) unstable; urgency=low

  * convert package to 3.0 (quilt) source format
  * Update location of upstream source
    + update d/copyright.
    + d/control: add new Homepage URL
    + fix d/watch to scan sf network (Closes: #460005)
  * Add git VCS headers to d/control
  * add missing format strings to fprintf() calls
  * move to debhelper v9
  * Minimize d/rules using dh(1)
  * fix spelling mistakes in documentation
  * bump std-ver to 3.9.4, no further changes needed

 -- Joe Nahmias <jello@debian.org>  Thu, 27 Jun 2013 18:22:16 -0400

vtprint (2.0.2-12) unstable; urgency=low

  * Remove -s from LINK_OPTS in upstream Makefile so that nostrip can work,
    closes: #438249.  Thanks for the report!
  * Bumped std-ver, no changes.

 -- Joe Nahmias <jello@debian.org>  Thu, 16 Aug 2007 22:11:32 -0400

vtprint (2.0.2-11) unstable; urgency=low

  * Updated maintainer email.
  * Bumped std-ver, no changes.

 -- Joe Nahmias <jello@debian.org>  Mon, 16 Feb 2004 20:17:01 -0500

vtprint (2.0.2-10) unstable; urgency=low

  * Upload sponsored by David Schleef <ds@schleef.org>
  * New Maintainer, closes: #120023.
  * Convert package to use debhelper.
  * debian/control (Std-Ver): 3.6.0.
  * debian/control (Build-Dep): added debhelper, groff; closes: #190617.

 -- Joe Nahmias <joe@nahmias.net>  Tue, 19 Aug 2003 01:19:21 -0400

vtprint (2.0.2-9) unstable; urgency=low

  * Per NMU Closes: Bug#91080, Bug#91684, Bug#100337
  * Remove emacs settings from changelog to quiet lintian
  * Orphan package

 -- Rich Sahlender <rsahlen@debian.org>  Sun, 18 Nov 2001 13:54:03 -0500

vtprint (2.0.2-8.1) unstable; urgency=low

  * FHS transition
  * Non maintainer Upload
  * a rebuild seems to have fixed the FHS issues
  * changed copyright file to reflect newer location of GPL
  * removed INSTALL file from /usr/share/doc/vtprint
  * added -isp flags to dpkg-gencontrol to create Section and Priority
    fields
  * used the patch from
      Stephen Stafford <bagpuss@debian.org>  Sat,  9 Jun 2001 22:29:41 +0100
    which
  * closes: #100337
    and
  * closes: #91684
    and
  * closes: #91080

 -- Andreas Tille <tille@debian.org>  Thu, 05 Jul 2001 01:20:15 +0200

vtprint (2.0.2-8) unstable; urgency=low

  * New Maintainer.

 -- Rich Sahlender <rsahlen@debian.org>  Sun,  8 Mar 1998 23:47:34 -0500

vtprint (2.0.2-7) unstable; urgency=low

  * Corrected year info in copyright file.
  * Updated to Standards-Version 2.4.0.0.

 -- Johnie Ingram <johnie@debian.org>  Tue,  3 Feb 1998 14:45:45 -0500

vtprint (2.0.2-6) unstable; urgency=low

  * Updated to Standards-Version 2.3.0.1 with debmake 3.5.8.

 -- Johnie Ingram <johnie@debian.org>  Tue,  3 Feb 1998 14:39:54 -0500

vtprint (2.0.2-5) unstable; urgency=low

  * Tweaked packaging for better multi-architecture support.
  * Switched to pristine source tar archive.
  * Updated to Standards-Version 2.2.0.0 with debmake 3.3.12.

 -- Johnie Ingram <johnie@debian.org>  Sun, 31 Aug 1997 01:08:34 -0400

vtprint (2.0.2-4) unstable; urgency=low

  * Updated to Standards-Version 2.1.3.2 with debmake 3.3.3.
  * Linked against libc6.

 -- Johnie Ingram <johnie@debian.org>  Sun,  1 Jun 1997 14:58:02 -0400

vtprint (2.0.2-3) stable unstable; urgency=low

  * Added file checksums and tweaked rules file for current debmake
    (3.2.2).
  
 -- Johnie Ingram <johnie@debian.org>  Mon, 30 Dec 1996 16:48:33 -0500

vtprint (2.0.2-2) stable unstable; urgency=low

  * Minor tweaks to copyright info and build procedure.
  * Updated to Standards-Version 2.1.2.2 with debmake 2.41.
  * Dependency changed to allow use of libc5 from stable distribution.
  * Fixed permissions on /etc/vtprintcap (no longer an executable).

 -- Johnie Ingram <johnie@debian.org>  Sun, 29 Dec 1996 01:34:09 -0500

vtprint (2.0.2-1) unstable; urgency=low

  * Initial Release.

 -- Johnie Ingram <johnie@debian.org>  Sun, 8 Dec 1996 23:31:09 -0500
